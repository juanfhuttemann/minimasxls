<?php
date_default_timezone_set('America/Asuncion');
$fecha = "'".date("Y-m-d")."'"; //Para el SELECT
$hora = "'".date("H:i")."'"; //Para el SELECT
$ahora = date("YmdHi"); //Para el nombre del Archivo
// es la hora local by juanhuttemann
$utcDif=4;
//


// Reporte de Errores
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Librería PHPExcel */
require_once('./Classes/PHPExcel.php');

// Crea el objeto PHPExcel
$objPHPExcel = new PHPExcel();

/** Conexión a la BD */
require_once('./database/connection.php');
require_once('./database/query.php');

$result = $con->query($sql);

if ($result->num_rows > 0) {
		$i=2;
    while($row = $result->fetch_assoc()) {
			$objPHPExcel->setActiveSheetIndex(0)

									//->setCellValue('B'.$i, $row["nombre"])
			            ->setCellValue('A'.$i, $row["latitud"])
			            ->setCellValue('B'.$i, $row["longitud"])
									->setCellValue('C'.$i, $row["temp_aire"])
									->setCellValue('D'.$i, $row["estacion"]);
									//->setCellValue('E'.$i, $row["fechadato"])
									//->setCellValue('F'.$i, date("H:i",date(strtotime($row["horadato"]))-3600*$utcDif))

				$i++;
    }
} else {
    echo "0 resultados";
}
$con->close();

// Propiedades del Documento
$objPHPExcel->getProperties()->setCreator("DMH")
							 ->setTitle("Reporte de Temperatura Mínima")
							 ->setSubject("Reporte de las Temperaturas Mínimas en estaciones automáticas del País");


/** Formato para Planilla */
// Encabezados
$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A1', 'Latitud')
						->setCellValue('B1', 'Longitud')
						->setCellValue('C1', 'Temperatura')
						->setCellValue('D1', 'Código');
						// ->setCellValue('B1', 'Localidad')
						// ->setCellValue('E1', 'Fecha')
						// ->setCellValue('F1', 'Hora')
						// ->setCellValue('G1', 'Temperatura');

//Titulos en Negrita
$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);


//Dimensiones en pixeles
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
// $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
// $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
// $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);

// Nombra la plantilla
$objPHPExcel->getActiveSheet()->setTitle('Minimas');

// Guarda el archivo en Excel 2007

//echo date('H:i:s') , " Archivo Generado Exitosamente!" , EOL;

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save($ahora.' minimas.xlsx');
